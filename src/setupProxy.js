const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/v2',
    createProxyMiddleware({
      target: 'http://t.yushu.im/',
      changeOrigin: true,
      pathRewrite: {
        "^/v2": "/"
      },
    })
  );
};