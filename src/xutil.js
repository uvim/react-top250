export const xutil = (function () {
    var util = {
        /**
         * localstorage 存储取值
         * @param namespace 
         * @param data
         * @param expires 过期时间，单位秒
         * @returns {*}
         */
        store: function (namespace, data, expires) {
            if (data) {
                if (expires > 0) {
                    data.expires = new Date().getTime() + expires * 1000;
                }
                return window.localStorage.setItem(namespace, JSON.stringify(data));
            }

            var store = window.localStorage.getItem(namespace);
            if (store) {
                let storeValue = JSON.parse(store);
                var currTime = new Date().getTime();
                if (storeValue.hasOwnProperty('expires') && currTime > storeValue.expires) {
                    window.localstorage.removeItem(namespace);
                    return [];
                }
            }
            return (store && JSON.parse(store)) || [];
        },
        createUUID: function () {
            var s = [], itoh = '0123456789ABCDEF';
            var i;
            for ( i = 0; i < 36; i++) {
                s[i] = Math.floor(Math.random()*0x10);
            }
            s[14] = 4;
            s[19] = (s[19] & 0x3) | 0x8;
            for ( i = 0; i <36; i++){

                s[i] = itoh[s[i]];
            }
            s[8] = s[13] = s[18] = s[23] = '-';

            return s.join('');
        },
        /**
         * Take an array and turn into a hash with even number arguments as keys and odd numbers as
         * values. Allows creating constants for commonly used style properties, attributes etc.
         * Avoid it in performance critical situations like looping
         */
        hash: function () {
            var i = 0,
                args = arguments,
                length = args.length,
                obj = {};
            for (; i < length; i++) {
                obj[args[i++]] = args[i];
            }
            return obj;
        },
        getQueryVariable: function (variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] === variable){return pair[1];}
            }
            return(false);
        },
        isChineseStr: function(str) {
            return new RegExp("[\u4E00-\u9FA5]+").test(str);
        },
        trim: function () {
            return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
        },
        isArray: function (arr) {
            return Object.prototype.toString.call(arr) === '[object Array]';
        },
        toThousands: function (num) {
            return parseFloat(num).toFixed(2).replace(/(\d{1,3})(?=(\d{3})+(?:\.))/g, "$1,");
        },
        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        getCookie: function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
        removeCookie: function (name) {
            this.setCookie(name, 1, -1);
        },
        getScrollOffset: function () {
            if(window.pageXOffset){
                return{
                    X : window.pageXOffset,
                    Y : window.pageYOffset
                }
            }else{ //<= IE8
                return{
                    X : document.body.scrollLeft+document.documentElement.scrollLeft,
                    Y : document.body.scrollTop+document.documentElement.scrollTop
                }
            }
        },
        // validateEmail: function (email) {
        //     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //     return re.test(String(email).toLowerCase());
        // },
        validateTel: function (str) {
            var result = str.match(/\d{3}-\d{8}|\d{4}-\d{7,8}/);
            if (result == null) return false;
            return true;
        },
        validateMobile: function (str) {
            if (!(/^1[3-9][0-9]\d{8}$/.test(str))) {
                return false;
            }
            return true;
        },
        isMobile: function() {
            return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
        }
    };

    return util;
})();