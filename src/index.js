import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import './movie.css';
import Movie from './Movie';

ReactDOM.render(<Movie />, document.getElementById('root'));